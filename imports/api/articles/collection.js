import {Random} from 'meteor/random';
import {Mongo} from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import {Images} from "/imports/api/images/collection";

SimpleSchema.extendOptions(['autoform']);

export const Articles = new Mongo.Collection('articles');

ImagesCollection = Images;

Articles.attachSchema(new SimpleSchema({
    'files': {
        type: Object,
        label: false,
        optional: true,
    },
    'files.image': {
        type: String,
        optional: true,
        label: "image",
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'ImagesCollection',
            }
        }
    },

    'data': {
        type: Object,
        label: false,
    },
    'data.name': {
        type: String,
        label: 'Name',
    },
}, {tracker: Tracker}));
