import {Articles} from '/imports/api/articles/collection';

Articles.permit(['insert', 'update', 'remove']).allowInClientCode();
