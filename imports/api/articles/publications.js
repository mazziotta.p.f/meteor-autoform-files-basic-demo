import {Articles} from "./collection";

Meteor.publish('articles', () => {
    return Articles.find({});
});
