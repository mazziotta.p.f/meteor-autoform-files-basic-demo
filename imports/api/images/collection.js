import {FilesCollection} from 'meteor/ostrio:files';

export const Images = new FilesCollection({
    collectionName: 'images',
    allowClientCode: true,
    onBeforeUpload(file) {
        // Allow upload files under 10MB, and only in png/jpg/jpeg formats
        if (file.size <= 10485760 && /png|jpg|jpeg|gif/i.test(file.extension)) {
            if (Meteor.isClient) Meteor.subscribe('imageByName', file.name);
            return true;
        } else {
            return 'Please upload image, with size equal or less than 10MB';
        }
    },
    storagePath: 'assets/uploads/images',
});