import {Images} from "./collection";

Meteor.publish('images', () => {
    return Images.collection.find({});
});
