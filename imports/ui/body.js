import './templates/default.html';

import {Articles} from "../api/articles/collection";
import {Images} from "../api/images/collection";

Router.route('/', {
    name: 'home',
    waitOn: () => [
        Meteor.subscribe('images'),
        Meteor.subscribe('articles'),
    ],
    data: {
        collection: Articles,
        articles: () => Articles.find(),
    }
});

Router.route('/add', {
    name: 'add',
    waitOn: () => [
        Meteor.subscribe('images'),
        Meteor.subscribe('articles'),
    ],
    data: {
        collection: Articles,
        articles: () => Articles.find(),
    }
});

Router.route('/edit/:_id', {
    name: 'edit',
    waitOn: () => [
        Meteor.subscribe('images'),
        Meteor.subscribe('articles'),
    ],
    data: {
        collection: Articles,
        doc: () => Articles.findOne({_id: Router.current().params._id}),
    }
});

AutoForm.addHooks('articleInsertForm', {
    after: {
        insert: (error, result) => error ? console.error(error) : Router.go('home'),
        update: (error, result) => error ? console.error(error) : Router.go('home'),
    },
});

AutoForm.addHooks('articleUpdateForm', {
    after: {
        insert: (error, result) => error ? console.error(error) : Router.go('home'),
        update: (error, result) => error ? console.error(error) : Router.go('home'),
    },
});

Template.home.helpers({
    imageLink() {
        if (!this.files || !this.files.image) return;

        const image = Images.findOne({_id: this.files.image});
        if (image) return image.link();
    }
})